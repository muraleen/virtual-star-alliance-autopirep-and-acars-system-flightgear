package csv;

// Author: Narendran Muraleedharan

import java.io.*;

public class CSVFile {

    public int columns;

    public String[][] strLine;
    public String curLine;

    public CSVFile(String filename, int col) throws FileNotFoundException, IOException {

        strLine = new String [5000][col];

        columns = col;

        FileInputStream fstream = new FileInputStream(filename);

        DataInputStream in = new DataInputStream(fstream);
        BufferedReader br = new BufferedReader(new InputStreamReader(in));

        int lineIndex = 0;

        while ((curLine = br.readLine()) != null)   {

            // Split line into separate data using comma as delimiter

            String[] splitLine = curLine.split(",");

            for(int n = 0; n < col; n++) {

                strLine[lineIndex][n] = splitLine[n];

            }
            
            lineIndex++;

        }

        in.close();

    }

    public String getValueAt(int row, int col) {

        return strLine[row][col];
        
    }

    public void printData() {

        for(int n = 0; strLine[n][0] != null; n++) {

            for(int m = 0; m < columns; m++) {

                System.out.print(strLine[n][m] + "  ");

            }

            System.out.println();

        }

    }

}