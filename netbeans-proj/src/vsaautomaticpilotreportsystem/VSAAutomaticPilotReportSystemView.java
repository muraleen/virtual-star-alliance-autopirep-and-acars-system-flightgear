package vsaautomaticpilotreportsystem;

/*
 * VSAAutomaticPilotReportSystemView.java
 */

import csv.CSVFile;

import java.text.DecimalFormat;

import java.lang.Math.*;

import org.jdesktop.application.Action;
import org.jdesktop.application.ResourceMap;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.FrameView;
import org.jdesktop.application.TaskMonitor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;
import javax.swing.Icon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import org.flightgear.fgfsclient.FGFSConnection;

import java.io.*;

import java.net.*;

import java.util.Vector;

// Parsing Stuff

import java.io.FileNotFoundException;
import javax.swing.DefaultComboBoxModel;

/**
 * The application's main frame.
 */
public class VSAAutomaticPilotReportSystemView extends FrameView {

    public static FGFSConnection fgfs;

    // FGLog Data

    public double StartTime;
    public double EndTime;
    public double StartFuel;
    public double EndFuel;

    // PIREP Data

    public String DepICAO; //Telnet Data
    public String ArrICAO; //Combo Box
    public String Aircraft; //Telnet Data
    public String FlightNum; //Combo Box
    public String Registration; //Combo Box
    public int PaxCount; //TextField
    public double TicketPrice; //CSV Table
    public String FlightTime; //PIREP End Flight
    public double FuelUsed; //PIREP End Flight
    // public double LandRate; //PIREP Touchdown

    public int AircraftID;

    // Define CSV Files

    CSVFile flights;
    CSVFile fleet;

    public VSAAutomaticPilotReportSystemView(SingleFrameApplication app) {
        super(app);

        initComponents();
        

        // status bar initialization - message timeout, idle icon and busy animation, etc
        ResourceMap resourceMap = getResourceMap();
        int messageTimeout = resourceMap.getInteger("StatusBar.messageTimeout");
        messageTimer = new Timer(messageTimeout, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                statusMessageLabel.setText("");
            }
        });
        messageTimer.setRepeats(false);
        int busyAnimationRate = resourceMap.getInteger("StatusBar.busyAnimationRate");
        for (int i = 0; i < busyIcons.length; i++) {
            busyIcons[i] = resourceMap.getIcon("StatusBar.busyIcons[" + i + "]");
        }
        busyIconTimer = new Timer(busyAnimationRate, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                busyIconIndex = (busyIconIndex + 1) % busyIcons.length;
                statusAnimationLabel.setIcon(busyIcons[busyIconIndex]);
            }
        });
        idleIcon = resourceMap.getIcon("StatusBar.idleIcon");
        statusAnimationLabel.setIcon(idleIcon);
        progressBar.setVisible(false);

        // connecting action tasks to status bar via TaskMonitor
        TaskMonitor taskMonitor = new TaskMonitor(getApplication().getContext());
        taskMonitor.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                String propertyName = evt.getPropertyName();
                if ("started".equals(propertyName)) {
                    if (!busyIconTimer.isRunning()) {
                        statusAnimationLabel.setIcon(busyIcons[0]);
                        busyIconIndex = 0;
                        busyIconTimer.start();
                    }
                    progressBar.setVisible(true);
                    progressBar.setIndeterminate(true);
                } else if ("done".equals(propertyName)) {
                    busyIconTimer.stop();
                    statusAnimationLabel.setIcon(idleIcon);
                    progressBar.setVisible(false);
                    progressBar.setValue(0);
                } else if ("message".equals(propertyName)) {
                    String text = (String)(evt.getNewValue());
                    statusMessageLabel.setText((text == null) ? "" : text);
                    messageTimer.restart();
                } else if ("progress".equals(propertyName)) {
                    int value = (Integer)(evt.getNewValue());
                    progressBar.setVisible(true);
                    progressBar.setIndeterminate(false);
                    progressBar.setValue(value);
                }
            }
        });
    }

    @Action
    public void showAboutBox() {
        if (aboutBox == null) {
            JFrame mainFrame = VSAAutomaticPilotReportSystemApp.getApplication().getMainFrame();
            aboutBox = new VSAAutomaticPilotReportSystemAboutBox(mainFrame);
            aboutBox.setLocationRelativeTo(mainFrame);
        }
        VSAAutomaticPilotReportSystemApp.getApplication().show(aboutBox);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jComboBox2 = new javax.swing.JComboBox();
        jComboBox3 = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jTextField14 = new javax.swing.JTextField();
        jComboBox4 = new javax.swing.JComboBox();
        jTextField4 = new javax.swing.JTextField();
        jButton5 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jTextField6 = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jTextField5 = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jTextField7 = new javax.swing.JTextField();
        jTextField8 = new javax.swing.JTextField();
        jTextField9 = new javax.swing.JTextField();
        jTextField10 = new javax.swing.JTextField();
        jTextField11 = new javax.swing.JTextField();
        jTextField12 = new javax.swing.JTextField();
        jTextField13 = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jTextField17 = new javax.swing.JTextField();
        jTextField18 = new javax.swing.JTextField();
        jTextField19 = new javax.swing.JTextField();
        jButton6 = new javax.swing.JButton();
        jLabel24 = new javax.swing.JLabel();
        jTextField20 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        statusPanel = new javax.swing.JPanel();
        javax.swing.JSeparator statusPanelSeparator = new javax.swing.JSeparator();
        statusMessageLabel = new javax.swing.JLabel();
        statusAnimationLabel = new javax.swing.JLabel();
        progressBar = new javax.swing.JProgressBar();
        jComboBox1 = new javax.swing.JComboBox();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jTextField15 = new javax.swing.JTextField();
        jTextField16 = new javax.swing.JTextField();
        jButton4 = new javax.swing.JButton();

        mainPanel.setName("mainPanel"); // NOI18N
        mainPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(vsaautomaticpilotreportsystem.VSAAutomaticPilotReportSystemApp.class).getContext().getResourceMap(VSAAutomaticPilotReportSystemView.class);
        jLabel1.setText(resourceMap.getString("jLabel1.text")); // NOI18N
        jLabel1.setName("jLabel1"); // NOI18N
        mainPanel.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 10, -1, 30));

        jTextField1.setText(resourceMap.getString("jTextField1.text")); // NOI18N
        jTextField1.setName("jTextField1"); // NOI18N
        mainPanel.add(jTextField1, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 10, 130, -1));

        jTabbedPane1.setName("jTabbedPane1"); // NOI18N

        jPanel1.setName("jPanel1"); // NOI18N
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel3.setText(resourceMap.getString("jLabel3.text")); // NOI18N
        jLabel3.setName("jLabel3"); // NOI18N
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 8, -1, 30));

        jLabel4.setText(resourceMap.getString("jLabel4.text")); // NOI18N
        jLabel4.setName("jLabel4"); // NOI18N
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 48, -1, 30));

        jLabel6.setText(resourceMap.getString("jLabel6.text")); // NOI18N
        jLabel6.setName("jLabel6"); // NOI18N
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 90, -1, 30));

        jTextField2.setText(resourceMap.getString("jTextField2.text")); // NOI18N
        jTextField2.setName("jTextField2"); // NOI18N
        jPanel1.add(jTextField2, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 88, 150, 30));

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "No Data" }));
        jComboBox2.setName("jComboBox2"); // NOI18N
        jComboBox2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox2ActionPerformed(evt);
            }
        });
        jPanel1.add(jComboBox2, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 50, 150, -1));

        jComboBox3.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "No Data" }));
        jComboBox3.setName("jComboBox3"); // NOI18N
        jComboBox3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox3ActionPerformed(evt);
            }
        });
        jPanel1.add(jComboBox3, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 90, 140, -1));

        jLabel7.setText(resourceMap.getString("jLabel7.text")); // NOI18N
        jLabel7.setName("jLabel7"); // NOI18N
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 8, -1, 30));

        jLabel20.setText(resourceMap.getString("jLabel20.text")); // NOI18N
        jLabel20.setName("jLabel20"); // NOI18N
        jPanel1.add(jLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 48, -1, 30));

        jLabel21.setText(resourceMap.getString("jLabel21.text")); // NOI18N
        jLabel21.setName("jLabel21"); // NOI18N
        jPanel1.add(jLabel21, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 90, -1, 30));

        jTextField14.setEditable(false);
        jTextField14.setText(resourceMap.getString("jTextField14.text")); // NOI18N
        jTextField14.setName("jTextField14"); // NOI18N
        jPanel1.add(jTextField14, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 10, 140, -1));

        jComboBox4.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "No Data", "xyz" }));
        jComboBox4.setName("jComboBox4"); // NOI18N
        jComboBox4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox4ActionPerformed(evt);
            }
        });
        jPanel1.add(jComboBox4, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 50, 140, -1));

        jTextField4.setEditable(false);
        jTextField4.setText(resourceMap.getString("jTextField4.text")); // NOI18N
        jTextField4.setName("jTextField4"); // NOI18N
        jPanel1.add(jTextField4, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 10, 150, -1));

        javax.swing.ActionMap actionMap = org.jdesktop.application.Application.getInstance(vsaautomaticpilotreportsystem.VSAAutomaticPilotReportSystemApp.class).getContext().getActionMap(VSAAutomaticPilotReportSystemView.class, this);
        jButton5.setAction(actionMap.get("resetData")); // NOI18N
        jButton5.setText(resourceMap.getString("jButton5.text")); // NOI18N
        jButton5.setName("jButton5"); // NOI18N
        jPanel1.add(jButton5, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 130, 150, -1));

        jTabbedPane1.addTab(resourceMap.getString("jPanel1.TabConstraints.tabTitle"), jPanel1); // NOI18N

        jPanel3.setName("jPanel3"); // NOI18N

        jTextField6.setEditable(false);
        jTextField6.setText(resourceMap.getString("jTextField6.text")); // NOI18N
        jTextField6.setName("jTextField6"); // NOI18N

        jButton2.setAction(actionMap.get("filePIREP")); // NOI18N
        jButton2.setText(resourceMap.getString("jButton2.text")); // NOI18N
        jButton2.setName("jButton2"); // NOI18N

        jLabel5.setText(resourceMap.getString("jLabel5.text")); // NOI18N
        jLabel5.setName("jLabel5"); // NOI18N

        jButton1.setAction(actionMap.get("endFlight")); // NOI18N
        jButton1.setText(resourceMap.getString("jButton1.text")); // NOI18N
        jButton1.setName("jButton1"); // NOI18N

        jButton3.setAction(actionMap.get("startFlight")); // NOI18N
        jButton3.setText(resourceMap.getString("jButton3.text")); // NOI18N
        jButton3.setName("jButton3"); // NOI18N

        jTextField5.setEditable(false);
        jTextField5.setText(resourceMap.getString("jTextField5.text")); // NOI18N
        jTextField5.setName("jTextField5"); // NOI18N

        jLabel9.setText(resourceMap.getString("jLabel9.text")); // NOI18N
        jLabel9.setName("jLabel9"); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 460, Short.MAX_VALUE)
                    .addComponent(jButton3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 460, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, 460, Short.MAX_VALUE)
                        .addGroup(jPanel3Layout.createSequentialGroup()
                            .addComponent(jLabel9)
                            .addGap(2, 2, 2)
                            .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jLabel5)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addGap(6, 6, 6)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton2)
                .addContainerGap(16, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab(resourceMap.getString("jPanel3.TabConstraints.tabTitle"), jPanel3); // NOI18N

        jPanel2.setName("jPanel2"); // NOI18N
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel12.setText(resourceMap.getString("jLabel12.text")); // NOI18N
        jLabel12.setName("jLabel12"); // NOI18N
        jPanel2.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 10, 80, 30));

        jLabel13.setText(resourceMap.getString("jLabel13.text")); // NOI18N
        jLabel13.setName("jLabel13"); // NOI18N
        jPanel2.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 50, 70, 30));

        jLabel14.setText(resourceMap.getString("jLabel14.text")); // NOI18N
        jLabel14.setName("jLabel14"); // NOI18N
        jPanel2.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 70, 30));

        jLabel15.setText(resourceMap.getString("jLabel15.text")); // NOI18N
        jLabel15.setName("jLabel15"); // NOI18N
        jPanel2.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 130, 90, 30));

        jLabel16.setText(resourceMap.getString("jLabel16.text")); // NOI18N
        jLabel16.setName("jLabel16"); // NOI18N
        jPanel2.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, 70, 30));

        jLabel17.setText(resourceMap.getString("jLabel17.text")); // NOI18N
        jLabel17.setName("jLabel17"); // NOI18N
        jPanel2.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 90, 70, 30));

        jLabel18.setText(resourceMap.getString("jLabel18.text")); // NOI18N
        jLabel18.setName("jLabel18"); // NOI18N
        jPanel2.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 90, 70, 30));

        jTextField7.setEditable(false);
        jTextField7.setText(resourceMap.getString("jTextField7.text")); // NOI18N
        jTextField7.setName("jTextField7"); // NOI18N
        jPanel2.add(jTextField7, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 10, 150, -1));

        jTextField8.setEditable(false);
        jTextField8.setText(resourceMap.getString("jTextField8.text")); // NOI18N
        jTextField8.setName("jTextField8"); // NOI18N
        jPanel2.add(jTextField8, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 90, 150, -1));

        jTextField9.setEditable(false);
        jTextField9.setText(resourceMap.getString("jTextField9.text")); // NOI18N
        jTextField9.setName("jTextField9"); // NOI18N
        jPanel2.add(jTextField9, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 10, 140, -1));

        jTextField10.setEditable(false);
        jTextField10.setText(resourceMap.getString("jTextField10.text")); // NOI18N
        jTextField10.setName("jTextField10"); // NOI18N
        jPanel2.add(jTextField10, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 130, 350, -1));

        jTextField11.setEditable(false);
        jTextField11.setText(resourceMap.getString("jTextField11.text")); // NOI18N
        jTextField11.setName("jTextField11"); // NOI18N
        jPanel2.add(jTextField11, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 50, 150, -1));

        jTextField12.setEditable(false);
        jTextField12.setText(resourceMap.getString("jTextField12.text")); // NOI18N
        jTextField12.setName("jTextField12"); // NOI18N
        jPanel2.add(jTextField12, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 50, 140, -1));

        jTextField13.setEditable(false);
        jTextField13.setText(resourceMap.getString("jTextField13.text")); // NOI18N
        jTextField13.setName("jTextField13"); // NOI18N
        jPanel2.add(jTextField13, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 90, 140, -1));

        jTabbedPane1.addTab(resourceMap.getString("jPanel2.TabConstraints.tabTitle"), jPanel2); // NOI18N

        jPanel4.setName("jPanel4"); // NOI18N
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel10.setText(resourceMap.getString("jLabel10.text")); // NOI18N
        jLabel10.setName("jLabel10"); // NOI18N
        jPanel4.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, -1, 30));

        jLabel11.setText(resourceMap.getString("jLabel11.text")); // NOI18N
        jLabel11.setName("jLabel11"); // NOI18N
        jPanel4.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, 30));

        jLabel19.setText(resourceMap.getString("jLabel19.text")); // NOI18N
        jLabel19.setName("jLabel19"); // NOI18N
        jPanel4.add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 100, -1, 30));

        jTextField17.setText(resourceMap.getString("jTextField17.text")); // NOI18N
        jTextField17.setName("jTextField17"); // NOI18N
        jPanel4.add(jTextField17, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 70, 320, -1));

        jTextField18.setText(resourceMap.getString("jTextField18.text")); // NOI18N
        jTextField18.setName("jTextField18"); // NOI18N
        jPanel4.add(jTextField18, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 10, 320, -1));

        jTextField19.setText(resourceMap.getString("jTextField19.text")); // NOI18N
        jTextField19.setName("jTextField19"); // NOI18N
        jPanel4.add(jTextField19, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 40, 320, -1));

        jButton6.setAction(actionMap.get("resetSettings")); // NOI18N
        jButton6.setText(resourceMap.getString("jButton6.text")); // NOI18N
        jButton6.setName("jButton6"); // NOI18N
        jPanel4.add(jButton6, new org.netbeans.lib.awtextra.AbsoluteConstraints(312, 130, 150, -1));

        jLabel24.setText(resourceMap.getString("jLabel24.text")); // NOI18N
        jLabel24.setName("jLabel24"); // NOI18N
        jPanel4.add(jLabel24, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, -1, 30));

        jTextField20.setText(resourceMap.getString("jTextField20.text")); // NOI18N
        jTextField20.setName("jTextField20"); // NOI18N
        jPanel4.add(jTextField20, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 100, 320, -1));

        jTabbedPane1.addTab(resourceMap.getString("jPanel4.TabConstraints.tabTitle"), jPanel4); // NOI18N

        jTabbedPane1.setSelectedIndex(3);

        mainPanel.add(jTabbedPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 80, 490, 210));
        jTabbedPane1.getAccessibleContext().setAccessibleName(resourceMap.getString("jTabbedPane1.AccessibleContext.accessibleName")); // NOI18N

        jLabel2.setText(resourceMap.getString("jLabel2.text")); // NOI18N
        jLabel2.setName("jLabel2"); // NOI18N
        mainPanel.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 50, -1, 30));

        statusPanel.setName("statusPanel"); // NOI18N

        statusPanelSeparator.setName("statusPanelSeparator"); // NOI18N

        statusMessageLabel.setName("statusMessageLabel"); // NOI18N

        statusAnimationLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        statusAnimationLabel.setName("statusAnimationLabel"); // NOI18N

        progressBar.setName("progressBar"); // NOI18N

        javax.swing.GroupLayout statusPanelLayout = new javax.swing.GroupLayout(statusPanel);
        statusPanel.setLayout(statusPanelLayout);
        statusPanelLayout.setHorizontalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(statusPanelSeparator, javax.swing.GroupLayout.DEFAULT_SIZE, 503, Short.MAX_VALUE)
            .addGroup(statusPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(statusMessageLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 317, Short.MAX_VALUE)
                .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(statusAnimationLabel)
                .addContainerGap())
        );
        statusPanelLayout.setVerticalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(statusPanelLayout.createSequentialGroup()
                .addComponent(statusPanelSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(statusMessageLabel)
                    .addComponent(statusAnimationLabel)
                    .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3))
        );

        mainPanel.add(statusPanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 290, -1, -1));

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Merlion Virtual Airlines", "Virtual Star Alliance" }));
        jComboBox1.setName("jComboBox1"); // NOI18N
        mainPanel.add(jComboBox1, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 50, 210, -1));

        jSeparator2.setName("jSeparator2"); // NOI18N
        mainPanel.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, 490, 10));

        jLabel22.setText(resourceMap.getString("jLabel22.text")); // NOI18N
        jLabel22.setName("jLabel22"); // NOI18N
        mainPanel.add(jLabel22, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, -1, 30));

        jLabel23.setText(resourceMap.getString("jLabel23.text")); // NOI18N
        jLabel23.setName("jLabel23"); // NOI18N
        mainPanel.add(jLabel23, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, 30));

        jTextField15.setText(resourceMap.getString("jTextField15.text")); // NOI18N
        jTextField15.setName("jTextField15"); // NOI18N
        mainPanel.add(jTextField15, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 50, 150, -1));

        jTextField16.setText(resourceMap.getString("jTextField16.text")); // NOI18N
        jTextField16.setName("jTextField16"); // NOI18N
        mainPanel.add(jTextField16, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 10, 130, -1));

        jButton4.setAction(actionMap.get("connectTelnet")); // NOI18N
        jButton4.setText(resourceMap.getString("jButton4.text")); // NOI18N
        jButton4.setName("jButton4"); // NOI18N
        mainPanel.add(jButton4, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 10, 120, -1));

        setComponent(mainPanel);
        setStatusBar(statusPanel);
    }// </editor-fold>//GEN-END:initComponents

    // Change Arrival Airport Action Handler

    private void jComboBox4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox4ActionPerformed

        ArrICAO = (String) jComboBox4.getSelectedItem();

        Vector comboBoxItems = new Vector();

        comboBoxItems.add("- Select -");

        // Create a list of available aircraft registrations

        for(int x = 0; flights.getValueAt(x, 0) != null; x++) {

            if((flights.getValueAt(x,3).equals(ArrICAO)) && (flights.getValueAt(x, 2).equals(DepICAO))) {

                AircraftID = Integer.parseInt(flights.getValueAt(x, 4));

                for(int y = 0; fleet.getValueAt(y, 0) != null; y++) {

                    if((AircraftID == Integer.parseInt(fleet.getValueAt(y, 0))) && (fleet.getValueAt(y, 1).equals(jTextField4.getText()))) {

                        comboBoxItems.add(fleet.getValueAt(y, 2));

                    }

                }

            }

        }

        final DefaultComboBoxModel model2 = new DefaultComboBoxModel(comboBoxItems);

        jComboBox2.setModel(model2);

    }//GEN-LAST:event_jComboBox4ActionPerformed

    private void jComboBox2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox2ActionPerformed

        // Create List of Flight Numbers when Registration is changed

        Registration = (String) jComboBox2.getSelectedItem();

        Vector comboBoxItems = new Vector();

        comboBoxItems.add("- Select -");

        for(int x = 0; flights.getValueAt(x, 0) != null; x++) {

            if((flights.getValueAt(x,3).equals(ArrICAO)) && (flights.getValueAt(x, 2).equals(DepICAO))) {

                AircraftID = Integer.parseInt(flights.getValueAt(x, 4));

                for(int y = 0; fleet.getValueAt(y, 0) != null; y++) {

                    if((AircraftID == Integer.parseInt(fleet.getValueAt(y, 0))) && (fleet.getValueAt(y, 2).equals(Registration))) {

                        comboBoxItems.add(flights.getValueAt(x, 0) + flights.getValueAt(x, 1));

                    }

                }

            }

        }

        final DefaultComboBoxModel model3 = new DefaultComboBoxModel(comboBoxItems);

        jComboBox3.setModel(model3);

    }//GEN-LAST:event_jComboBox2ActionPerformed

    private void jComboBox3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox3ActionPerformed

        // Get Ticket Price for Flight to calculate revenue

        FlightNum = (String) jComboBox3.getSelectedItem();

        for(int x = 0; flights.getValueAt(x, 0) != null; x++) {

            if (("MIA" + flights.getValueAt(x, 1)).equals(FlightNum)) {

                TicketPrice = Double.parseDouble(flights.getValueAt(x, 5));

                // System.out.print(TicketPrice);

            }

        }

    }//GEN-LAST:event_jComboBox3ActionPerformed

    @Action
    public void connectTelnet() throws IOException {

        String host = jTextField16.getText();
        int port = Integer.parseInt(jTextField1.getText());

        fgfs = new FGFSConnection(host, port);

        // Get Departure Airport and Aircraft

        DepICAO = fgfs.get("/sim/airport/closest-airport-id");
        Aircraft = fgfs.get("/sim/aero");

        jTextField14.setText(DepICAO);
        jTextField4.setText(Aircraft);

        parseArrList(DepICAO, Aircraft);

    }

    public void parseArrList(String dep, String ac) throws FileNotFoundException, IOException {

        System.out.println("-----------------------------------");
        System.out.println("Schedules File Source: " + jTextField18.getText() + "/VSA_s.csv");
        System.out.println("Aircraft File Source: " + jTextField18.getText() + "/VSA_a.csv");

        Vector comboBoxItems = new Vector();

        comboBoxItems.add("- Select -");

        // Parse CSV Files

        flights = new CSVFile(jTextField18.getText() + "/VSA_s.csv", 6);
        fleet = new CSVFile(jTextField18.getText() + "/VSA_a.csv", 3);

        // flights.printData(); This was simply to test my CSV parser

        /*
         * VSA_s.csv Columns:
         *
         * 0- Airline Code
         * 1- Flight Num <- PRIMARY INDEX
         * 2- DepICAO
         * 3- ArrICAO
         * 4- Aircraft ID
         * 5- Ticket Price
         *
         * VSA_a.csv Columns:
         *
         * 0- Aircraft Index
         * 1- Aircraft Name (aero)
         * 2- Aircraft Registration
         *
         */

        for(int x = 0; flights.getValueAt(x, 0) != null; x++) {

            if (flights.getValueAt(x,2).equals(dep)) {

                AircraftID = Integer.parseInt(flights.getValueAt(x,4));

                for(int y = 0; fleet.getValueAt(y, 0) != null; y++) {

                    if((Integer.parseInt(fleet.getValueAt(y, 0)) == AircraftID) && (fleet.getValueAt(y,1).equals(ac))) {

                            comboBoxItems.add(flights.getValueAt(x, 3));

                    }

                }

            }

        }
        
        final DefaultComboBoxModel model = new DefaultComboBoxModel(comboBoxItems);

        jComboBox4.setModel(model);

    }

    @Action
    public void resetSettings() {

        jTextField18.setText("APS_Data");
        jTextField19.setText("http://merlionvirtual.clubos.net/autopirep.php");
        jTextField17.setText("http://virtualstaralliance.cer33.com/acars.php");
        jTextField20.setText("http://virtualstaralliance.cer33.com/acars_end.php");

    }

    @Action
    public void resetData() throws IOException {

        // Get Departure Airport and Aircraft

        DepICAO = fgfs.get("/sim/airport/closest-airport-id");
        Aircraft = fgfs.get("/sim/aero");

        jTextField14.setText(DepICAO);
        jTextField4.setText(Aircraft);

        parseArrList(DepICAO, Aircraft);

        jComboBox4.setSelectedIndex(0);

    }

    @Action
    public void startFlight() throws IOException, InterruptedException {

        // Get Initial Data for PIREP

        PaxCount = Integer.parseInt(jTextField2.getText());
        StartTime = fgfs.getDouble("/sim/time/elapsed-sec");
        StartFuel = fgfs.getDouble("/consumables/fuel/total-fuel-kg");

        jTabbedPane1.setSelectedIndex(2);

        // Initialize some variables for ACARS

        int altitude = 0;
        double vs = 0;
        double latitude = 0;
        double longitude = 0;
        int gspeed = 0;
        double etd = 0;
        double eta = 0;
        String etdS = "";
        String etaS = "";
        int headingInt = 0;

        // Run ACARS Loop till your closest airport is the Arrival airport and you touchdown

        while((!InRange()) || (!TouchDown())) {

            altitude = (int) fgfs.getDouble("/position/altitude-ft");
            vs = fgfs.getDouble("/velocities/vertical-speed-fps");
            latitude = fgfs.getDouble("/position/latitude-deg");
            longitude = fgfs.getDouble("/position/longitude-deg");
            gspeed = (int) fgfs.getDouble("/velocities/groundspeed-kt");

            headingInt = (int) fgfs.getDouble("/orientation/heading-deg");

            String distString = fgfs.get("/autopilot/route-manager/wp-last/dist");

            if (!distString.equals("")) {

                etd = roundTD(fgfs.getDouble("/autopilot/route-manager/wp-last/dist"));

                eta = roundTD(etd / gspeed);

                // Converting ETA from Actual Decimal to Hour and Minute

                int minute = (int) (eta * 60);

                int hour = minute / 60;

                int minleft = minute - (hour * 60);

                etdS = Double.toString(roundTD(etd)) + " nm";

                if (minleft > 9)
                    etaS = Integer.toString(hour) + ":" + Integer.toString(minleft);
                else
                    etaS = Integer.toString(hour) + ":0" + Integer.toString(minleft);

                jTabbedPane1.setSelectedIndex(2);

            } else {

                etdS = "---.--";
                etaS = "--:--";

            }

            // Set Variables into TextFields

            jTextField9.setText(Double.toString(roundTD(latitude)));
            jTextField7.setText(Double.toString(roundTD(longitude)));

            String AltString = altitude + " ft";
            String GSString = gspeed + " kts";

            jTextField12.setText(AltString);
            jTextField11.setText(GSString);

            jTextField13.setText(etdS);
            jTextField8.setText(etaS);

            // Check Flight Phase

            if (vs > 10)
                jTextField10.setText("Climb");
            else if ((altitude > 10000) && (Math.abs(vs) < 10))
                jTextField10.setText("Cruise");
            else if ((altitude > 8000) && (vs < -10))
                jTextField10.setText("Descent");
            else if ((altitude <= 8000) && (vs < -5))
                jTextField10.setText("Approach");

            sendACARSData(latitude, longitude, altitude, gspeed, etdS, etaS, headingInt);

            Thread.sleep(120000);

        }

        // The loops ends when the aircraft arrives

        jTextField10.setText("Landed");

        sendACARSData(latitude, longitude, altitude, gspeed, etdS, etaS, headingInt);


    }

    public void sendACARSData(double lat, double lon, int alt, int gspeed, String etd, String eta, int heading) throws MalformedURLException, IOException {

        URL url = new URL(jTextField17.getText());

        String result = "";

        String String1 = "lat=" + URLEncoder.encode(Double.toString(lat), "UTF-8");

        String String2 = "lon=" + URLEncoder.encode(Double.toString(lon), "UTF-8");

        String String3 = "alt=" + URLEncoder.encode(Integer.toString(alt), "UTF-8");

        String String4 = "gs=" + URLEncoder.encode(Integer.toString(gspeed), "UTF-8");

        String String5 = "etd=" + URLEncoder.encode(etd, "UTF-8");

        String String6 = "eta=" + URLEncoder.encode(eta, "UTF-8");

        String String7 = "phase=" + URLEncoder.encode(jTextField10.getText(), "UTF-8");

        String String8 = "fnum=" + URLEncoder.encode((String) jComboBox3.getSelectedItem(), "UTF-8");

        String String9 = "dep=" + URLEncoder.encode(DepICAO, "UTF-8");

        String String10 = "arr=" + URLEncoder.encode(ArrICAO, "UTF-8");

        String String11 = "id=" + URLEncoder.encode(jTextField15.getText(), "UTF-8");

        String String12 = "hdg=" + URLEncoder.encode(Integer.toString(heading), "UTF-8");

        String data = String1 + "&" + String2 + "&" + String3 + "&" + String4 + "&" + String5 + "&" + String6 + "&" + String7 + "&" + String8 + "&" + String9 + "&" + String10 + "&" + String11 + "&" + String12;

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        try {

            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");

            // Send the POST data
            DataOutputStream dataOut = new DataOutputStream(
            connection.getOutputStream());
            dataOut.writeBytes(data);
            dataOut.flush();
            dataOut.close();

            DataInputStream in = new DataInputStream (connection.getInputStream ()); // same connection

            String g;
            while ((g = in.readLine()) != null) {
                result += g;
            }
            in.close();

        } finally {
            connection.disconnect();
            System.out.println(result);
        }

    }

    public double roundTD(double d) {

            DecimalFormat twoDForm = new DecimalFormat("#.##");

        return Double.valueOf(twoDForm.format(d));
}

    public boolean InRange() throws IOException {

        String ClosestICAO = fgfs.get("/sim/airport/closest-airport-id");

        System.out.println("Closest ICAO: " + ClosestICAO);
        System.out.println("Arrival ICAO: " + ArrICAO);

        if (ClosestICAO.equals(ArrICAO)) {

            return true;

        } else {

            return false;

        }

    }

    public boolean TouchDown() throws IOException {

        String wowString = fgfs.get("/gear/gear[1]/wow");

        boolean wow = false;

        if (wowString.equals("0") || wowString.equals("1")) {

            wow = (wowString.equals("1"));

        } else {

            wow = (wowString.equals("true"));

        }

        if (wow)
            System.out.println("Aircraft is on the Ground");
        else
            System.out.println("Aircraft is airbornce");

        return wow;

    }

    @Action
    public void endFlight() throws IOException {

        EndTime = fgfs.getDouble("/sim/time/elapsed-sec");
        EndFuel = fgfs.getDouble("/consumables/fuel/total-fuel-kg");

        int timeMinute = (int) ((EndTime - StartTime) / 60);

        int timeHour = timeMinute / 60;

        int timeMinLeft = timeMinute - (timeHour * 60);

        if (timeMinLeft > 9)
            FlightTime = timeHour + "." + timeMinLeft;
        else
            FlightTime = timeHour + ".0" + timeMinLeft;

        FuelUsed = StartFuel - EndFuel;

        jTextField5.setText(FlightTime);
        jTextField6.setText(Double.toString(roundTD(FuelUsed)));

        URL url = new URL(jTextField20.getText());

        String result = "";

        String data = "pilotid=" + URLEncoder.encode(jTextField15.getText(), "UTF-8");

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        try {

            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");

            // Send the POST data
            DataOutputStream dataOut = new DataOutputStream(
            connection.getOutputStream());
            dataOut.writeBytes(data);
            dataOut.flush();
            dataOut.close();

            DataInputStream in = new DataInputStream (connection.getInputStream ()); // same connection

            String g;
            while ((g = in.readLine()) != null) {
                result += g;
            }
            in.close();

        } finally {
            connection.disconnect();
            System.out.println(result);
        }

    }

    @Action
    public void filePIREP() throws UnsupportedEncodingException, MalformedURLException, IOException {

        URL url = new URL(jTextField19.getText());

        String result = "";

        String String1 = "pilotid=" + URLEncoder.encode(jTextField15.getText(), "UTF-8");

        String String2 = "flightnum=" + URLEncoder.encode((String) jComboBox3.getSelectedItem(), "UTF-8");

        String String3 = "dep=" + URLEncoder.encode(DepICAO, "UTF-8");

        String String4 = "arr=" + URLEncoder.encode(ArrICAO, "UTF-8");

        String String5 = "time=" + URLEncoder.encode(FlightTime, "UTF-8");

        String String6 = "fuel=" + URLEncoder.encode(Double.toString(FuelUsed), "UTF-8");

        String String7 = "price=" + URLEncoder.encode(Double.toString(TicketPrice), "UTF-8");

        String String8 = "pax=" + URLEncoder.encode(String.valueOf(PaxCount), "UTF-8");

        String String9 = "ac=" + URLEncoder.encode(Integer.toString(AircraftID), "UTF-8");

        String data = String1 + "&" + String2 + "&" + String3 + "&" + String4 + "&" + String5 + "&" + String6 + "&" + String7 + "&" + String8 + "&" + String9;

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        try {

            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");

            // Send the POST data
            DataOutputStream dataOut = new DataOutputStream(
            connection.getOutputStream());
            dataOut.writeBytes(data);
            dataOut.flush();
            dataOut.close();

            DataInputStream in = new DataInputStream (connection.getInputStream ()); // same connection

            String g;
            while ((g = in.readLine()) != null) {
                result += g;
            }
            in.close();

        } finally {
            connection.disconnect();
            System.out.println(result);
        }

    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JComboBox jComboBox2;
    private javax.swing.JComboBox jComboBox3;
    private javax.swing.JComboBox jComboBox4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField10;
    private javax.swing.JTextField jTextField11;
    private javax.swing.JTextField jTextField12;
    private javax.swing.JTextField jTextField13;
    private javax.swing.JTextField jTextField14;
    private javax.swing.JTextField jTextField15;
    private javax.swing.JTextField jTextField16;
    private javax.swing.JTextField jTextField17;
    private javax.swing.JTextField jTextField18;
    private javax.swing.JTextField jTextField19;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField20;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JTextField jTextField6;
    private javax.swing.JTextField jTextField7;
    private javax.swing.JTextField jTextField8;
    private javax.swing.JTextField jTextField9;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JLabel statusAnimationLabel;
    private javax.swing.JLabel statusMessageLabel;
    private javax.swing.JPanel statusPanel;
    // End of variables declaration//GEN-END:variables

    private final Timer messageTimer;
    private final Timer busyIconTimer;
    private final Icon idleIcon;
    private final Icon[] busyIcons = new Icon[15];
    private int busyIconIndex = 0;

    private JDialog aboutBox;
}
